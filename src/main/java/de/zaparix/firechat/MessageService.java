package de.zaparix.firechat;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by luca on 18.06.17.
 */
@Service
public class MessageService {

    public interface MessageListener {

        void messageReceived(Message message);
    }

    protected final List<MessageListener> listeners = new ArrayList<>();

    public void addListener(MessageListener listener) {
        listeners.add(listener);
    }

    public void removeListener(MessageListener listener) {
        listeners.remove(listener);
    }

    public void sendMessage(Message message) {

    }

}
