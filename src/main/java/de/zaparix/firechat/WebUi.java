package de.zaparix.firechat;

import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;

/**
 * Created by luca on 18.06.17.
 */
@SpringUI
public class WebUi extends UI {

    protected VerticalLayout rootLayout;

    protected VerticalLayout messengerView;

    protected TextArea chatTextArea;

    protected HorizontalLayout messageSendLayout;

    protected TextField messageTextField;

    protected Button messageSendButton;

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        initLayout();
    }

    protected void initLayout() {
        initRootLayout();

        setContent(rootLayout);
    }

    private void initRootLayout() {
        initMessengerView();

        rootLayout = new VerticalLayout(messengerView);
        rootLayout.setComponentAlignment(messengerView, Alignment.MIDDLE_CENTER);
    }

    private void initMessengerView() {
        initChatTextArea();
        initMessageSendLayout();

        messengerView = new VerticalLayout(chatTextArea, messageSendLayout);
        messengerView.setWidth(600, Unit.PIXELS);
        messengerView.setHeight(800, Unit.PIXELS);
    }

    private void initChatTextArea() {
        chatTextArea = new TextArea();
        chatTextArea.setSizeFull();
    }

    private void initMessageSendLayout() {
        initMessageTextField();
        initMessageSendButton();

        messageSendLayout = new HorizontalLayout(messageTextField, messageSendButton);
        messageSendLayout.setWidth(100, Unit.PERCENTAGE);
        messageSendLayout.setSpacing(true);
        messageSendLayout.setExpandRatio(messageTextField, 1F);
    }

    private void initMessageTextField() {
        messageTextField = new TextField();
        messageTextField.setWidth(100, Unit.PERCENTAGE);
    }

    private void initMessageSendButton() {
        messageSendButton = new Button("Send");
        messageSendButton.setStyleName(ValoTheme.BUTTON_PRIMARY);
    }

}
