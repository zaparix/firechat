package de.zaparix.firechat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

/**
 * A message entity for the chat application.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Message {

    private String id;

    private LocalDate date;

    private String text;

}
