package de.zaparix.firechat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FireChatApplication {

	public static void main(String[] args) {
		SpringApplication.run(FireChatApplication.class, args);
	}
}
